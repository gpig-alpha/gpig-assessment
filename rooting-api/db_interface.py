import psycopg2
import pandas as pd
import time
import re
import os
import os.path
import pickle
import math
import yaml
import argparse


class Way:
    def __init__(self, _id, setup, start, end, length):
        self.way_id = _id
        self.meta_way_id = setup["way_id"]
        self.start_node = start
        self.end_node = end
        self.length = length
        self.building_density = setup["building_density"]
        self.maxspeed = setup["maxspeed"]
        self.weight = setup["weight"]
        self.lanes = setup["lanes"]
        self.highway = setup["highway"]
        self.sidewalk = setup["sidewalk"]

    def get_insert_sql(self):

        return "INSERT INTO split_ways VALUES ({}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, '{}', '{}');".format(
            self.way_id,
            self.meta_way_id,
            self.start_node.nodeID,
            self.start_node.get_lat_lon(),
            self.end_node.nodeID,
            self.end_node.get_lat_lon(),
            self.weight,
            self.building_density,
            self.length,
            self.lanes,
            self.maxspeed,
            self.highway,
            self.sidewalk
        )

    def __str__(self):
        return "{}.({}). From {} to {} with weight {} and length {}".format(self.way_id, self.meta_way_id, self.start_node.nodeID, self.end_node.nodeID, self.weight, self.length)


class Node:
    def __init__(self, nodeID, latitude, longitude):
        self.nodeID = nodeID
        self.lat = latitude
        self.lon = longitude

    def __str__(self):
        return "{} at ({},{})".format(self.nodeID, self.lat, self.lon)

    def get_lat_lon(self):
        return "'{ %f, %f }'" % (self.lat, self.lon)

    def find_length(self, other):
        degrees_to_mile_const = 69
        return math.sqrt((abs(self.lat - other.lat) ** 2) + abs(self.lon - other.lon) ** 2) * degrees_to_mile_const


class DBInterface:

    def __init__(self, con, config):
        self.data_path = './data.csv'
        self.all_ways = []
        self.parsed_data = {}
        self.con = con
        self.config = config

        # Get way data from the database
        data = pd.read_sql_query("SELECT * FROM ways", self.con)
        self.data = data[['id', 'nodes', 'tags']].values

        # Get node data and create a dict of {node_id: [lat, lon], ...} 
        nodes = pd.read_sql_query("SELECT * FROM nodes", self.con)
        self.nodes = self.transform_nodes(nodes[['id', 'tags']].values)

    @staticmethod
    def get_val_from_tag_string(tag, string, default):
        splt = string.split('=>')
        try:
            ind = splt.index([x for x in splt if tag in x][0]) + 1
            return (splt[ind].split('"')[1])
        except:
            return default

    def check_for_node_in_other_ways(self, key, node):
        is_in = False
        for k, v in self.parsed_data.items():
            if key != k:
                if node.nodeID in [x.nodeID for x in v["nodes"]]:
                    is_in = True
                    break
        return is_in

    def build_node_information(self, node_ids):
        nodes = []
        for node_id in node_ids:
            try:
                lat, lon = self.nodes[node_id]
                nodes.append(Node(node_id, lat, lon))
            except:
                print("Probably couldnt find the node id in the self.nodes")
        return nodes

    def transform_nodes(self, nodes):
        node_dict = {}
        for i in range(len(nodes)):
            tag_str = nodes[i][1]
            lat = float(self.get_val_from_tag_string("lat", tag_str, 0))
            lon = float(self.get_val_from_tag_string("lon", tag_str, 0))
            node_dict[int(nodes[i][0])] = [lat, lon]
        return node_dict

    def parse_ways_tags(self, data):
        config = self.config['way_properties']
        for orig_way in data:
            ## Get Way ID
            _id = int(orig_way[0])

            ## Get Nodes and create Node classes
            node_ids = [int(x) for x in str(orig_way[1]).replace("[", "").replace("]", "").replace(" ", "").split(",")]
            nodes = self.build_node_information(node_ids)

            ## Parse tags
            tags = orig_way[2]
            building_density = float(self.get_val_from_tag_string("building_density", tags, 1))
            road_length = float(self.get_val_from_tag_string("road_length", tags, 1))
            maxspeed = int(self.get_val_from_tag_string("maxspeed", tags, "20").replace(" mph", ""))
            oneway = str(self.get_val_from_tag_string("oneway", tags, "no")) == "yes"
            junction = str(self.get_val_from_tag_string("junction", tags, ""))
            highway = str(self.get_val_from_tag_string("highway", tags, "road"))
            sidewalk = str(self.get_val_from_tag_string("sidewalk", tags, "none"))
            lanes = int(self.get_val_from_tag_string("lanes", tags, 2))
            oneway = oneway or junction == "roundabout"

            # {way_id: {dict of way properties}, ...}
            self.parsed_data[_id] = {
                "way_id": _id,
                "nodes": nodes,
                "building_density": building_density,
                "road_length": road_length,
                "maxspeed": maxspeed,
                "oneway": oneway,
                "junction": junction,
                "highway": highway,
                "sidewalk": sidewalk,
                "lanes": lanes,
                "weight": 0.0
            }


    def build_ways(self):
        # Parse ways and form dictionary for each way
        self.parse_ways_tags(self.data)
        
        # Go through and create a Way class for each little way
        _id = 0
        for key, value in self.parsed_data.items():
            start_node = value["nodes"][0]
            end_node = None
            for node in value["nodes"]:
                if node.nodeID != start_node.nodeID:
                    # Check if this node is present in another way or if it's the last node
                    if self.check_for_node_in_other_ways(key, node) or node.nodeID == value["nodes"][-1].nodeID:
                        end_node = node
                        self.all_ways.append(Way(_id, value, start_node, end_node, start_node.find_length(end_node)))
                        if not value["oneway"]:
                            _id += 1
                            self.all_ways.append(Way(_id, value, end_node, start_node, start_node.find_length(end_node)))
                        start_node = node
                        end_node = None
                        _id += 1


        with open('./ways.pickle', 'wb') as f:
            pickle.dump(self.all_ways, f)


    def run(self):
        self.build_ways()
        return list(self.all_ways)


## Config and Utility functions
def get_config():
    with open('./properties.yml', 'r') as file:
        config = yaml.load(file, Loader=yaml.FullLoader)
    return config


def get_db_conn():
    con = psycopg2.connect(
        host = os.environ['HOST'],
        port = os.environ['PORT'],
        database = os.environ['DATABASE'],
        user = os.environ['USER'],
        password = os.environ['PASSWORD']
    )
    print(os.environ['HOST'])
    return con
###############################################



## API-called functions  
def update_database():
    try:
        config = get_config()
        with get_db_conn() as con:
            db_int = DBInterface(con, config)
            ways = db_int.run()
            sql_command = "DELETE FROM split_ways;\n"
            for way in ways:
                sql_command += "{}\n\n".format(way.get_insert_sql())
            cursor = con.cursor()
            cursor.execute(sql_command)
            cursor.close()
        return True
    except Exception as e:
        return "Exception occured: {}".format(e)

def get_data_for_split_way_row_where(column_name, value):
    config = get_config()
    with get_db_conn() as con:
        cursor = con.cursor()
        sql_command = "SELECT * FROM split_ways"
        if column_name != "*" and value != "*":
            sql_command += " WHERE {}={}".format(column_name, value)
        cursor.execute(sql_command)
        data = cursor.fetchall() 
        cursor.close()
    return data


def update_data_for_split_way_row_where(set_col, set_val, cond_col, con_val):
    config = get_config()
    with get_db_conn() as con:
        cursor = con.cursor()
        sql_command = "UPDATE split_ways SET {} = {} WHERE {} = {}".format(set_col, set_val, cond_col, con_val)
        cursor.execute(sql_command)
        sql_command = "SELECT * FROM split_ways WHERE {} = {}".format(cond_col, con_val)
        cursor.execute(sql_command)
        data = cursor.fetchall() 
        cursor.close()
    return data

###############################################
