from db_interface import db_interface
from rooter import Graph, dijkstra, shortest
import argparse

def rootingengine(start, end):
    db = db_interface(should_fetch)
    ways = db.run()

    # database mapping to Graph
    g = Graph()

    g.load_data(ways)

    print('Graph data:')
    for v in g:
        for w in v.get_connections():
            vid = v.get_id()
            wid = w.get_id()
            # print('( %s , %s, %3d)'  % ( vid, wid, v.get_weight(w)))

    try:    
        dijkstra(g, g.get_vertex(start), g.get_vertex(end)) 

        target = g.get_vertex(end)
        path = [target.get_id()]
        shortest(target, path)
        print('The shortest path : %s' %(path[::-1]))
    except:
        print("Either the start or end node id doesn't exist")

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Routing from Start to Finish.')
    parser.add_argument('-s', '--start', type=int, help="Start node id", default=259178656)
    parser.add_argument('-e', '--end', type=int, help="End node id", default=257923782)
    parser.add_argument('-f', '--fetch', help="Re-fetch data from database", action='store_true')

    args = parser.parse_args()
    start = int(args.start)
    end = int(args.end)
    should_fetch = bool(args.fetch)
    rootingengine(start, end, should_fetch)


## Examples
# start = 259178656
# end = 257923782
