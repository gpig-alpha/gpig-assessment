# Rooting and Ways API

Go to http://host:5000/ to view the endpoints:
```
endpoints = {
    "home": {"endpoint": '/', 
            "description": "Displays all the endpoints available",
            "example": "http://host:5000/"
            },
    "UpdateAll": {"endpoint": '/update_full_database', 
                  "description": "Updates the database. Returns 200 if successful",
                  "example": "http://host:5000/update_full_database"},
    "DBAPI": {"endpoint": '/access_ways',
        "get": {
            "parameters": {"column_name": "provide column name for WHERE clause", "value": "provide value that column_name should equal. If set to *, all is returned"}, 
            "description": "Returns row with WHERE clause. If column_name or value aren't included or marked as *, then all rows are returned",
            "example": "http://host:5000/access_ways?column_name=lanes&value=2"},
        "put": {"parameters": {"set_col": "provide column name for SET clause", 
                                          "set_val": "provide value for SET clause",
                                          "cond_col": "provide column name for WHERE clause", 
                                          "cond_val": "provide value for WHERE clause"},
                "description": "Formated as: UPDATE split_ways SET set_col = set_val WHERE cond_col = cond_val, Returns the row if successful",
                "example": "http://host:5000/access_ways?set_col=lanes&set_val=2&cond_col=way_id&con_val=0"},
    }
}
```

This service is built from the Dockerfile and is run from the docker-compose. It only runs the development server as it's a prototype. This would have to change for full deployments.