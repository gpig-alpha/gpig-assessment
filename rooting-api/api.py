import os

from flask import Flask, request, jsonify
from flask_restful import Resource, Api

from db_interface import update_database, get_data_for_split_way_row_where, update_data_for_split_way_row_where

app = Flask(__name__)
api = Api(app)

endpoints = {
    "home": {"endpoint": '/', 
            "description": "Displays all the endpoints available",
            "example": "http://host:5000/"
            },
    "UpdateAll": {"endpoint": '/update_full_database', 
                  "description": "Updates the database. Returns 200 if successful",
                  "example": "http://host:5000/update_full_database"},
    "DBAPI": {"endpoint": '/access_ways',
        "get": {
            "parameters": {"column_name": "provide column name for WHERE clause", "value": "provide value that column_name should equal. If set to *, all is returned"}, 
            "description": "Returns row with WHERE clause. If column_name or value aren't included or marked as *, then all rows are returned",
            "example": "http://host:5000/access_ways?column_name=lanes&value=2"},
        "put": {"parameters": {"set_col": "provide column name for SET clause", 
                                          "set_val": "provide value for SET clause",
                                          "cond_col": "provide column name for WHERE clause", 
                                          "cond_val": "provide value for WHERE clause"},
                "description": "Formated as: UPDATE split_ways SET set_col = set_val WHERE cond_col = cond_val, Returns the row if successful",
                "example": "http://host:5000/access_ways?set_col=lanes&set_val=2&cond_col=way_id&con_val=0"},
    }
}

class Home(Resource):
    def get(self):
        return {"endpoints": endpoints}


class FullDBUpdate(Resource):
    def get(self):
        try:
            update_database()
        except Exception as e:
            return {"Error": e}
        return {200: "Successfully Updated"}


class DBAPI(Resource):
    def get(self):
        column_name = "*"
        value = "*"
        if 'column_name' in request.args: column_name = str(request.args['column_name'])
        if 'value' in request.args: value = str(request.args['value'])

        try:
            rows = get_data_for_split_way_row_where(column_name, value)
            return jsonify(rows)
        except Exception as e:
            return {"Exception": e}

    def put(self):
        set_col, set_val, cond_col, con_val = None, None, None, None
        if 'set_col' in request.args: 
            set_col = str(request.args['set_col'])
        if 'set_val' in request.args: 
            set_val = str(request.args['set_val'])
        if 'cond_col' in request.args: 
            cond_col = str(request.args['cond_col'])
        if 'con_val' in request.args: 
            con_val = str(request.args['con_val'])

        if not (set_col and set_val and cond_col and con_val):
            return {"Error": "Need: set_col, set_val, cond_col and con_val parameters set"}

        try:
            rows = update_data_for_split_way_row_where(set_col, set_val, cond_col, con_val)
            return jsonify(rows)
        except Exception as e:
            return {"Exception": e}

api.add_resource(Home, endpoints["home"]['endpoint'])
api.add_resource(FullDBUpdate, endpoints["UpdateAll"]['endpoint'])
api.add_resource(DBAPI, endpoints["DBAPI"]['endpoint'])

if __name__ == '__main__':
    host = '0.0.0.0'
    if host == '127.0.0.1':
        os.environ.setdefault('HOST', '104.197.194.54')
        os.environ.setdefault('PORT', '5431')
        os.environ.setdefault('USER', 'postgres')
        os.environ.setdefault('DATABASE', 'map')
        os.environ.setdefault('PASSWORD', 'postgres')
    app.run(debug=True, host=host)