# Database

This is a Postgres database that has the following extentions:

1. postgis
2. hstore
3. postgis_topology
4. fuzzystrmatch
5. postgis_tiger_geocoder

The initdb-postgis.sh script does:

1. Download extentions
2. Run setup of tables and functions for osmosis
3. Run setup of split_ways table

The update-postgis.sh script updates the extentions and versions should the postgres version change.

After importing a map to the database, the ways table is populated like this:
![Alt text](../media/ways_table.PNG "Ways Table")

Once the parser is run from the API, the split_ways table is populated like this:
![Alt text](../media/split_ways_table.PNG "Ways Table")
