# GPIG assessment

This repo includes several modules:

1. Database (POSTGIS)
2. Web API
4. Graphhopper
5. Adminer (database UI)

The docker compose file will spin up these instances

## Web API

See rooting-api/README.md \
URL:  http://host:5000

## Graphhopper

URL:       host:8989 \
URL admin: host:8990

If at the /maps urls, it comes up with black tiles, click on the layers icon in 
the top right hand corner and click on the OpenStreetMap option. York map should
then appear, taken automatically from ./data/maps/mapmod.osm.pbf file.

See the graphhopper/README.md for more information.


## Adminer

URL:       host:8080 \
1. Login: select type PostgreSQL
2. All other login variables: postgres
3. Once logged in, make DB dropdown 'map' and schema 'public'

## Postgres Database

See the database/README.md for more information. \
URL:  http://host:5432


## Scripts 
#### ./scripts/import.sh

Takes in 1 argument, the OSM file name, and saves it to the map database

#### ./scripts/export.sh

Takes ian optional argument, the output OSM file name.
If none specified then it stores it to the input directory for graphhopper volume. If specified, then it outputs to the chosen file path.

#### ./scripts/restart-docker.sh

A script to help restart the docker container. Note this will wipe the volumes, and setup the database like new. You must run input script afterwards.

#### ./scripts/full-setup.sh

A script that stops all docker images, converts the osm file, uploads to the database, exports to the graphhopper dir and starts graphhopper

#### ./scripts/graphhopper-map-update.sh

A script that stops graphhopper, runs the export then start graphhopper again

#### ./scripts/backup.sh

A script that runs backup of the postgres database held in the docker container to the /backups/**.sql folder in the host. It also gets rid of anything that was saved before today so if anything wants kept, rename it to **.sql.old
