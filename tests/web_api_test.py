import requests
import sys

URL = "http://104.197.194.54:9002/access_ways"

if sys.argv[1] == "get":
    GETPARAMS = {"column_name":sys.argv[2], "value":sys.argv[3]}
    get = requests.get(url = URL, params = GETPARAMS)
    print(get.json())
elif sys.argv[1] == "put":
    PUTPARAMS = {"set_col":sys.argv[2], "set_val":sys.argv[3], "cond_col":sys.argv[4], "con_val":sys.argv[5]}
    put = requests.put(url = URL, params = PUTPARAMS)
    print(put.json())
elif sys.argv[1] == "validateput":
    try:
        GETPARAMS = {"column_name":sys.argv[4], "value":sys.argv[5]}
        original = requests.get(url = URL, params = GETPARAMS)
        PUTPARAMS = {"set_col":sys.argv[2], "set_val":sys.argv[3], "cond_col":sys.argv[4], "con_val":sys.argv[5]}
        new = requests.put(url = URL, params = PUTPARAMS)
        if new.json() == original.json():
            print("value unchanged")
            print(new.json())
        else:
            print("Column '" + sys.argv[2] + "' updated to value '" + sys.argv[3] + "' where column '" + sys.argv[4] + "' equals '"  + sys.argv[5] + "'")
            print(new.json())
    except:
        print("Error in validation") 