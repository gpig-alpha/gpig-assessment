#!/bin/bash
# $1 is the map file (as .osm)
if [ -z "$1" ]
then
        echo "Usage: full-setup.sh <input_map_name.osm file>"
else
        echo "converting file and saving to pbf"
        osmconvert $1 -o=$1.pbf
        echo "Restarting docker postgres and adminer"
        docker-compose -f ../docker-compose.yml kill
        echo "System pruning"
        docker system prune
        echo "Building and up for postgres and adminer"
        docker-compose -f ../docker-compose.yml build postgres adminer
        docker-compose -f ../docker-compose.yml up -d postgres adminer
        echo "sleep"
        sleep 10
        echo "osmosis to database"
        sudo osmosis --read-pbf $1.pbf --log-progress --write-pgsql database=map user=postgres password=postgres host=localhost:5431
        sleep 5
        echo "Exporting from database"
        osmosis --read-pgsql host=localhost:5431 database=map user=postgres password=postgres --dataset-dump --write-xml file=/app/gpig-assessment/data/maps/final.osm
        osmconvert /app/gpig-assessment/data/maps/final.osm -o=/app/gpig-assessment/data/maps/final.pbf
        echo "Building and up to Graphhopper"
        docker-compose -f ../docker-compose.yml build graphhopper
        docker-compose -f ../docker-compose.yml up -d
fi
