echo "must be run with admin priveledges"
docker-compose -f ../docker-compose.yml kill
docker system prune
docker-compose -f ../docker-compose.yml build
docker-compose -f ../docker-compose.yml up -d
echo "Database and Graphhopper now running"
echo "Use import and export scripts with osm file"