#!/bin/bash
# $1 is the output osm file
if [ -z "$1" ]
then
        echo "No output file given, exporting to Graphhopper input dir"
        osmosis --read-pgsql host=localhost:5431 database=map user=postgres password=postgres --dataset-dump --write-xml file=/app/gpig-assessment/data/maps/final.osm
else
        osmosis --read-pgsql host=localhost:5431 database=map user=postgres password=postgres --dataset-dump --write-xml file=$1
fi
