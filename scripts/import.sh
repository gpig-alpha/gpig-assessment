#!/bin/bash
# $1 is the map file (as .osm)
if [ -z "$1" ]
then
        echo "Usage: import.sh <map_name.osm file>"
else
        osmconvert $1 -o=$1.pbf
        sudo osmosis --read-pbf $1.pbf --log-progress --write-pgsql database=map user=postgres password=postgres host=localhost:5431
fi
