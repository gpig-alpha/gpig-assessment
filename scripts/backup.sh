find /backups/ -mtime +1 -name '*.sql' -exec rm -rf {} \;
docker exec -t gpigassessment_postgres_1  pg_dumpall -c -U postgres > /backups/dump_`date +%d-%m-%Y"_"%H_%M_%S`.sql