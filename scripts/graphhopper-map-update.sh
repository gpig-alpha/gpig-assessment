#!/bin/bash
docker-compose -f ../docker-compose.yml down graphhopper
sleep 5
osmosis --read-pgsql host=localhost:5431 database=map user=postgres password=postgres --dataset-dump --write-xml file=/app/gpig-assessment/data/maps/final.osm
osmconvert /app/gpig-assessment/data/maps/final.osm -o=/app/gpig-assessment/data/maps/final.pbf
sleep 5
docker-compose -f ../docker-compose.yml build graphhopper
docker-compose -f ../docker-compose.yml up -d